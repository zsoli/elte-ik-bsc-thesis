% =========================================
%                CHAPTER              
% =========================================
\chapter{Fejlesztői dokumentáció} % Developer guide
\label{ch:impl}

A dolgozat célja egy olyan program létrehozása, amely a következő szempontoknak megfelel:

\begin{itemize}
	\item DPDK felhasználása a kernel megkerülésére, így növelve a teljesítményt (lásd: \ref{fig:dpdk-kernel}. ábra).

	\item A csomagfeldolgozás működése könnyedén módosítható/konfigurálható szabályokkal.
	Ehhez a szabványos iptables szabály formátum használható.

	\bigskip

	\begin{figure}[H]
		\centering
		\includegraphics[width=0.5\textwidth]{dpdk-kernel}
		\caption{Felhasználói program kommunikációja a hálózati kártyával DPDK használata esetén.}
		\label{fig:dpdk-kernel}
	\end{figure}

	\bigskip

	\item Videókártyán dolgozza fel a beérkező hálózati csomagokat, amelyhez a GPU-k teljesítményét megpróbálja kiaknázni.
	Az ehhez szükséges technológiákat a DPDK és az Nvidia által fejlesztett \texttt{librte-nv} DPDK könyvtár adja (lásd: \ref{fig:libs}. ábra).
\end{itemize}

\bigskip

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{libs}
	\caption{A program által közvetlenül felhasznált könyvtárak.}
	\label{fig:libs}
\end{figure}


% =========================================
%                SECTION
% =========================================
\section{Forráskód áttekintés}

A dpdk-gpu-packet-processor program C és CUDA/C nyelven írt forráskódjai a \texttt{gpu-dpdk/examples/gpu-packet-processor} könyvtárban találhatók meg.
A teszteléshez felhasznált segéd Scapy\footnote{Scapy: Python könrnyezet és könyvtár hálózati csomagok manipulálására.} Python szkriptek a \texttt{gpu-dpdk/scapy-scripts} könyvtárban érhetőek el.
A fordítás menetét definiáló fájl, a \texttt{meson.build} Meson fájl, mellyel részletesen a \ref{ch:meson}-os fejezet foglalkozik.

A dpdk-gpu-packet-processor program belépési pontja a \texttt{main.c} fájl \texttt{main} függvénye, melyben a szükséges inicializációk után a hálózati eszköz és az RX/TX magok (lásd: \ref{ch:rxtx}-es fejezet) elindítása megtörténik, majd végül ezek leállítása után az erőforrások elengedése is megtörténik.


% =========================================
%                 SECTION
% =========================================
\section{RX/TX magok}
\label{ch:rxtx}

Egy RX/TX mag olyan CPU-n futó kód, amely RX esetén a csomagok beérkezéskor történő feldolgozásáért, TX esetén a csomagok adott portra történő kiírásáért felel.
Egy RX/TX mag, egy fizikai CPU magnak felel meg, továbbá az RX magok száma megegyezik a TX magok számával, így a programhoz rendelt CPU magok száma páros kell hogy legyen, amennyiben a megadott CPU magok száma nagyobb mint egy.
Speciálisan indítható a program egy CPU maggal is, ebben az esetben egy RX/TX pár fog futni a megadott CPU magon.

A bejövő csomagok memória helyeire mutatókat tárolunk az \texttt{\detokenize{nv_burst_obj}} objektumokban, amelyeket az \texttt{\detokenize{nv_queue}} objektum aggregál.
Ez az \texttt{\detokenize{nv_burst_obj}} egy ciklikus (vagy gyűrű) puffer, azaz ha a puffer végére értünk, akkor a következő elem a puffer legelső eleme lesz.
Minden mag számon tartja, hogy éppen a pufferben melyik elemet szeretné feldolgozni (azaz mely csomag halmazon dolgozik), és megpróbálja az éppen aktuális mutatót használni a csomagok feldolgozására.
Ha éppen az adott elemet egy másik mag már használja, akkor a következő elemre lép, és így tovább.
Tehát az RX/TX magok ciklikusan dolgozzák fel a pufferekben tárolt csomagokat, így biztosítva, hogy nem alakul ki verseny helyzet az adatokért.


% =========================================
%                SUBSECTION
% =========================================
\subsection{RX magok}

Az RX mag a csomagfeldolgozás első állomása. 
Egy RX mag, a csomagokat löketekben (\enquote{burst}) \footnote{löketekben: sorozatosan csoportokban.} olvassa be az aktuális pufferbe amíg el nem éri a specifikált csomag mennyiséget, majd átadja azokat egy CUDA kernelnek\footnote{CUDA kernel: GPU-n futó kódrészlet.} (bővebben a CUDA kernelekkel a \ref{ch:cuda-definitions} fejezet foglalkozik) GPU-n történő feldolgozásra.
A \ref{src:rx-read}-és kód részlet mutatja a bejövő csomagok beolvasását az aktuális pufferbe.

Az RX magot a \texttt{main.c} fájl \texttt{\detokenize{static void rx_core(int port_id, long queue_idx)}} függvénye írja le.

\bigskip

\lstset{caption={RX mag: csomagok beolvasása.}, label=src:rx-read}
\begin{lstlisting}[language={C}]
nb_rx = rte_eth_rx_burst(port_id, queue_idx, (struct rte_mbuf **)(rx_nv_obj[queue_idx]->hdr_burst_h), rx_nv_obj[queue_idx]->rxtx_burst);

while (!force_quit && nb_rx < (rx_nv_obj[queue_idx]->rxtx_burst - DRIVER_MIN_RX_PKTS)) {
    nb_rx += rte_eth_rx_burst(port_id, queue_idx, (struct rte_mbuf **)(&(rx_nv_obj[queue_idx]->hdr_burst_h[nb_rx])), (rx_nv_obj[queue_idx]->rxtx_burst - nb_rx));
}
\end{lstlisting}

\bigskip
Majd a csomagok elküldését GPU-n történő feldolgozásra a \ref{src:rx-gpu}-és kódrészletben található függvényhívás végzi.

\bigskip

\lstset{caption={RX mag: csomagok GPU-n történő feldolgozásának megkezdése.}, label=src:rx-gpu}
\begin{lstlisting}[language={C}, float, floatplacement=H]
nvl2fwd_process_gpu((l2fwd_burst_use_payload == 1 ? nvobj_get_burst_pl_dptr(rx_nv_obj[queue_idx]) : nvobj_get_burst_hdr_dptr(rx_nv_obj[queue_idx])),
    rx_nv_obj[queue_idx]->curr_mbuf_burst,
    l2fwd_burst_use_payload,
    rx_nv_obj[queue_idx]->stream,
    l2fwd_use_cuda_event == 1 ? NULL : rx_nv_obj[queue_idx]->sync_flag,
    l2fwd_mac_update,
    MAC_CUDA_BLOCKS,
    MIN(MAC_THREADS_BLOCK, nb_rx),
    fw);
\end{lstlisting}


% =========================================
%                SUBSECTION
% =========================================
\subsection{TX magok}

A TX mag a csomagfeldolgozás utolsó állomása. 
Egy TX mag a GPU-n történő feldolgozás után, az aktuális pufferben tárolt csomagokat löketekben kiküldi a NIC egy adott portján.
A mag logikájának leglényegesebb része a csomagok kiküldése, amit az \ref{src:tx-write}-as kódrészlet mutat be.

A TX magot a \texttt{main.c} fájl \texttt{\detokenize{static void tx_core(int port_id, long queue_idx)}} függvénye írja le.

\bigskip

\lstset{caption={TX mag: csomagok kiírása portra.}, label=src:tx-write}
\begin{lstlisting}[language={C}]
nb_tx = rte_eth_tx_burst(port_id, queue_idx, tx_mbufs, tx_pkts);
while (1 && !force_quit) {
    if (nb_tx < tx_pkts) {
        nb_tx += rte_eth_tx_burst(port_id, queue_idx, &(tx_mbufs[nb_tx]), tx_pkts - nb_tx);
    }

    if (nb_tx == tx_pkts)
        break;
}
\end{lstlisting}


% =========================================
%                 SECTION
% =========================================
\section{Használt CUDA fogalmak}
\label{ch:cuda-definitions}

További fejezetek, illetve a program kódjának megértése miatt szükséges kódban használt CUDA programozáshoz tartozó technikák, illetve fogalmak megértése, melyhez segítséget ez a fejezet nyújt. 

A CUDA függvények a következő kulcsszavakkal láthatóak el:

\begin{itemize}
	\item \texttt{\detokenize{__forceinline__}}:
	A standard C \texttt{inline} kulcsszóval analóg viselkedést idéz elő.

	\item \texttt{\detokenize{__global__}}:
	Ilyen függvényt csak host oldalról lehet meghívni, a \text{<{}<{}<...>{}>{}>} szimbólumok felhasználásával.
	Csak device függvények hívhatóak meg, kivéve a \texttt{printf{}} speciálisan implementált függvényt. 
	Az ilyen CUDA függvényt szokás CUDA kernelnek is hívni.

	\item \texttt{\detokenize{__device__}}:
	Az ún. \enquote{device} kód, amely a GPU-n fut.
	Olyan CUDA függvény, amelyet csak kernelből, vagy egy másik \texttt{\detokenize{__device__}} kulcsszóval ellátott függvényből lehet meghívni.
	A \texttt{\detokenize{__global__}} függvényekhez hasonlóan csak device függvények hívhatóak meg.

	\item \texttt{\detokenize{__host__}}:
	A \enquote{host} kód, a CPU-n futtatható program kód.
	Olyan CUDA függvény, amelyet nem lehet device kódból meghívni.
\end{itemize}

A \texttt{\detokenize{__host__}} és \texttt{\detokenize{__device__}} kulcsszavak kombinálhatóak. 
Ha egy \texttt{\detokenize{__host__}} \texttt{\detokenize{__device__}} függvényt definiálunk, akkor az meghívható host és device kódból is.
Ekkor használható a fügvényben a \texttt{\detokenize{__CUDA_ARCH__}} preprocesszor változó definiáltságának vizsgálata arra, hogy eldöntsük a kód CPU-n vagy GPU-n fut. (lásd: \ref{src:cuda-arch} példa kódrészlet.)

\bigskip

\lstset{caption={\texttt{\_\_CUDA\_ARCH\_\_} preprocesszor változó használata.}, label=src:cuda-arch}
\begin{lstlisting}[language={C}]
#ifdef __CUDA_ARCH__
	KLOG(DEBUG, "running on GPU\n");
#else
	LOG(DEBUG, "running on CPU\n");
#endif
\end{lstlisting}

\bigskip

\todo{needs verification}
Mivel a CUDA fordító egy C++ fordító, így a C függvények nevén is név dekorációt\footnote{Függvény név dekoráció: a program fordító által használt technika, amely módosítja a függvények nevét, hogy azok egyediek legyenek.} (vagy \enquote{mangling}) fog végezni, ezért nem definiált függvény hibákat kapunk fordításkor.
Ennek elkerülésére használható az \texttt{extern "C"} linkelési specifikáció, amely arra utasítja a fordítót, hogy ne végezzen név dekorációt a függvényeken.
Ennek használatát a \ref{src:cuda-include}-ös kódrészlet mutatja be.

\bigskip

\lstset{caption={extern "C" használata CUDA forrás fájlokban.}, label=src:cuda-include}
\begin{lstlisting}[language={C}, floatplacement=H]
#ifdef __cplusplus
extern "C" {
#endif

#include ...
...

#ifdef __cplusplus
}
#endif
\end{lstlisting}

\bigskip

\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{cuda-blocks}
	\caption{Blokkok végrehajtása 2, illetve 4 Streaming Multiprocessor-os (SM) GPU esetén (CUDA dokumentáció \cite{cuda-docs} alapján).}
	\label{fig:cuda-scalability}
\end{figure}

\bigskip

Minden Nvidia GPU egy, vagy több Streaming Multiprocessor-ból (SM) áll \cite{cuda-docs}, amelyek CUDA-ban a párhuzamosan futtatható szálak blokkjait végrehajtják (ld.: \ref{fig:cuda-scalability} illetve \ref{fig:cuda-thread-blocks} ábrák).

\bigskip

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{blocks-of-threads}
	\caption{Blokkok, illetve azok szálainak szerveződése (CUDA dokumentáció \cite{cuda-docs} alapján).}
	\label{fig:cuda-thread-blocks}
\end{figure}

CUDA kernel hívásnál (ld.: \ref{src:cuda-kernel-invokation}-as példa kódrészlet) a \texttt{<{}<{}<...>{}>{}>} szimbólumok között megadott paraméterek, a CUDA paraméterek.
Az első paraméter a blokkok számát adja meg, a második a blokkonkénti szálak számát, a harmadik paraméter a dinamikusan lefoglalt osztott memória méretét adja meg blokkonként byte méretben.
Az utolsó paraméter opcionális, amely a CUDA streamek számát adja meg.
Ha ezt nem adjuk meg manuálisan, az értéke automatikusan egy lesz.

\bigskip

\lstset{caption={CUDA kernel invokáció.}, label=src:cuda-kernel-invokation}
\begin{lstlisting}[language={C}]
kernel_function <<<blocks, threads, memory, streams>>>
				(function_arguments ...);
\end{lstlisting}

\bigskip

A CUDA eszköztára több lehetőséget is ad a fejlesztők számára a GPU memória kezelésének megoldására.
Az alapvető opció, a manuális memória kezelés, amelynek keretein belül a fejlesztőnek saját maga kell megoldania a GPU, illetve CPU közötti adat mozgatását.
Mivel ez a módszer nagy mértékben növeli a memóriakezelést komplex adatszerkezetek esetén, más módszer került felhasználásra.
A dpdk-gpu-packet-processor az úgy nevezett Unified Memory memóriakezelési rendszert (Mark Harris cikke \cite{cuda-unified-memory} alapján) használja, amely lehetővé teszi, hogy egy memória címről elérhesse az ott tárolt adatot a CPU és a GPU is.
Ebben az esetben a rendszer a háttérben automatikusan mozgatja a CPU és GPU közötti megosztando adatokat.
Ez a programozás szempontjából azt jelenti, hogy a normál \texttt{cudaMalloc} hívásokat \texttt{cudaMallocManaged} hívásokra kell cserélni, az ezzel járó további kód egyszerűsítések (manuális adat mozgatás nem szükséges) mellett.

Az említett CUDA fogalmakat és függvényeket részletesen ismerteti a CUDA dokumentáció és API leírás \cite{cuda-docs}.

% =========================================
%                 SECTION
% =========================================
\section{Tűzfal objektum}

Az objektumot, amely a tűzfal elemeit tárolja, a csomagok feldolgozásának megkezdése előtt fel kell építeni.
A létrehozási folyamat belépési pontja az \texttt{\detokenize{fw_conf_parser.c}} fájl \texttt{\detokenize{nv_fw_parser_init_from_file}} függvénye, amely gondoskodik az objektum inicializálásáról, és a megadott tűzfal konfigurációs fájl feldolgozásáról.

Az \texttt{\detokenize{nv_firewall}} a fő tűzfal objektum, amely a láncokat (\texttt{\detokenize{nv_firewall_chain}}) és a recent modul listáit (\texttt{\detokenize{module_recent_lists}}) tárolja.
Minden lánc objektum tárolja a saját nevét, illetve a tárolt szabályokat, és ehhez hasonlóan, minden recent lista objektum tárolja a nevét, illetve a benne feljegyezett IP címeket.
Ezt a szerveződést a \ref{fig:firewall-objects}-es ábra mutatja vizuálisan.

\bigskip

\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{nv_array}
	\caption{Tűzfal objektumok szerveződése.}
	\label{fig:nv-array}
\end{figure}

\bigskip

Az adatszerkezet, amelyben a láncok és a recent modul listái tárolódnak, az úgy nevezett \texttt{\detokenize{nv_array}} objektum, amely egy dinamikusan újraméretezhető vektor (lásd: \ref{fig:nv-array}-as ábra).

\bigskip

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{firewall_objects}
	\caption{Tűzfal objektumok szerveződése.}
	\label{fig:firewall-objects}
\end{figure}


% =========================================
%                 SECTION
% =========================================
\section{CUDA device kód áttekintése}

A GPU-n történő feldolgozás belépési pontja a \texttt{kernel.cu} fájl \texttt{\detokenize{nvl2fwd_launch_gpu_processing}} függvénye.
Ennek a függvénynek az egyetlen célja a fő CUDA kernel meghívása, amelyet speciálisan kell megtenni, az \ref{src:main-kernel-call}-es forráskódban leírtak szerint.

A program egyszerűsítési szempontok miatt, a blokkok számát 1-re korlátozza, dinamikusan lefoglalt osztott memóriára pedig nincsen szükség.

\bigskip

\todo[inline]{MAC\_THREADS\_BLOCK = 256 ? burst\ size vs cuda\_threads check}

\lstset{caption={A fő CUDA kernel hívása.}, label=src:main-kernel-call}
\begin{lstlisting}[language={C}]
gpu_main_kernel <<<cuda_blocks, cuda_threads, 0, stream>>> (i_ptr, nb_rx, sync_flag, nv_fw);
\end{lstlisting}

\bigskip

A meghívott kernelnek a következő paramétereket adjuk át:

\begin{compactitem}
    \item \texttt{i\_ptr}:
    Ez egy olyan mutató, amely a GPU memóriájában tárolt csomag pufferekre mutat.

    \item \texttt{nb\_rx}:
    Csomagok száma. Minden csomaghoz egy szálat rendelünk feldolgozáshoz.
    
    \item \texttt{sync\_flag}:
    Ez a változó adja meg, hogy szeretnénk-e szinkronizálni a kernelek futását.
    Ha igen, akkor a kernelek megvárják egymást terminálás előtt.

    \item \texttt{nv\_fw}:
    Mutató a GPU-n tárolt tűzfal objektumra, amely tárolja a tűzfal teljes konfigurációját.
\end{compactitem}

A fő CUDA kernel minden csomagra meghívja a \text{firewall.cu} fájl \texttt{\detokenize{nv_firewall_dev_proc_packet}} függvényét, amely érdemben elvégzi a csomag feldolgozását.
Ha feldolgozás végére nem dobtuk el a csomagot, akkor a kernel a csomag ethernet keretében található forrás MAC címét felcseréli a cél MAC címével, így indikálva, hogy a csomagot feldolgoztuk.

Az \texttt{\detokenize{nv_firewall_dev_proc_packet}} függvény segéd struktúrákba (lásd: \ref{fig:kernel-helpers}-ös ábra) dolgozza fel a csomagokat, majd elkezdi a tűzfal szűrok illeszkedésének vizsgálatát a \texttt{\detokenize{nv_firewall_dev_rule_match}} függvény többszöri meghívásával, a megfelelő tűzfal lánc mutatójával paraméterezve (kezdve az \texttt{INPUT} lánc mutatójával), amíg szükséges.

\bigskip

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{packet_helpers}
	\caption{Csomagok feldolgozásához használt segéd struktúrák.}
	\label{fig:kernel-helpers}
\end{figure}

\begin{note}
	A \ref{fig:kernel-helpers}-as ábrán látható \texttt{in\_addr\_t} típus előjeltelen 32 bites egész szám típus, a \texttt{in\_port\_t} pedig 16 bites előjeltelen egész szám típus.
	Az \texttt{rte\_} prefixummal ellátott struktúrák DPDK adatszerkezetek csomag fejlécek tárolásához, melyeknek használatát a DPDK API leírás részletez \cite{dpdk-api}.
\end{note}

% \lstset{caption={Kernelben használt segéd struktúrák.}, label=src:kernel-helpers}
% \begin{lstlisting}[language={C}]
% struct packet {
% 	struct rte_mbuf* mbuf;
% 	struct rte_ether_hdr *ether_hdr;
% 	struct rte_ipv4_hdr *ipv4_hdr;
% 	struct rte_udp_hdr *udp_hdr;
% 	struct rte_tcp_hdr *tcp_hdr;
% };

% struct packet_values {
% 	in_addr_t src_ip;
% 	in_addr_t dst_ip;
% 	in_port_t sport;
% 	in_port_t dport;
% };
% \end{lstlisting}

\bigskip

A \texttt{\detokenize{nv_firewall_dev_rule_match}} függvény elvégzi a szükséges illeszkedési vizsgálatokat, melyek alapján meghívhatja a \texttt{module\_recent.cu} fájlban definiált recent modul kernel függvényeket, illetve eldobhatja a csomagot, vagy beállíthatja a következő tűzfal láncot, melynek alapján a csomagfeldolgozás folytatódni fog.


% =========================================
%                 SECTION
% =========================================
\section{Meson konfiguráció}
\label{ch:meson}

A \texttt{meson.build} fájl definiálja a dpdk-gpu-packet-processor program fordításának folyamatát.
Ebben a fájlban kell megadni a C forrás fájlokat, amelyek a program részeit képezik, illetve a CUDA/C forrás fájlokat (\texttt{.cu} kiterjesztésű fájlok), amelyeket egy külön valtozóban kell megadni (mint az látszik a \ref{src:meson-sources} kódrészletben), mivel ezeket külön kell kezelni a nem CUDA forrásoktól.

\bigskip

\lstset{caption={Fordítás: forrás fájlok megadása}, label=src:meson-sources}
\begin{lstlisting}[language={Python}]
sources = files('main.c', 'nv_queue.c', 'utils.c', 'firewall.c', 'fw_conf_parser.c', 'module_recent.c')
nv_sources_in = ['kernel.cu', 'module_recent.cu', 'firewall.cu']
\end{lstlisting}

A CUDA forrás fájlokból először az \texttt{nvcc} fordítóprogrammal objektum fájlokat generálunk \texttt{.o} kiterjesztéssel, majd ezeket az objektum fájlokat összelinkeljük az \texttt{nvlink} programmal.
Végül az elkészült CUDA objektum fájlokat linkeljük a standard C forráskódok objektum fájljaival.
A folyamatot a \ref{fig:nvcc-separate-compilation}. ábra mutatja be.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{separate-compilation}
	\caption{CUDA forrás fájlok linkelésének folyamata C forrás fájlokkal (CUDA dokumentáció \cite{cuda-docs} alapján).}
	\label{fig:nvcc-separate-compilation}
\end{figure}

\bigskip

Ennek a megvalósításához a Meson build rendszer által rendelkezésünkre álló \texttt{custom\_target} metódusok használhatóak, mint az látható a  \ref{src:meson-custom-targets}. forrás részletben

\bigskip

\lstset{caption={Fordítás: Meson metódusok használata CUDA/C fordításra.}, label=src:meson-custom-targets}
\begin{lstlisting}[language={Python}]
cuda_obj_files = custom_target('cuda sources',
    input: [nv_sources_in],
    output: [nv_sources_out],
    command: [nvcc, '--device-c', '@INPUT@', '-odir', '@OUTDIR@', nvcc_flags])

cuda_linked_obj_file = custom_target('linked cuda src',
    input: [cuda_obj_files],
    output: 'linked_cuda_objs.o',
    command: [nvcc, '-dlink', '@INPUT@', '--output-file', '@OUTPUT@', nvcc_flags])

sources += [cuda_obj_files, cuda_linked_obj_file]
\end{lstlisting}

Utolsó lépésként a \ref{src:meson-custom-targets}-ban szereplő \texttt{sources} változó tartalma átadásra kerül a megadott C fordító számára, amely a megszokott működés szerint fordítja a programot.


% =========================================
%                 SECTION
% =========================================
\section{Logolás}
Mivel a device kódok logolása nem megoldható a rendelkezésünkre álló beépített DPDK log rendszerrel, a device és host kódok logolása két külön rendszerre van szétszedve.


% =========================================
%                SUBSECTION
% =========================================
\subsection{Host logolás}

A host kódok logolása a DPDK log rendszerével történik.
DPDK-ban a fejlesztők rendelkezésére áll több log kategória, amelyeket szabadon lehet felhasználni saját kategóriák létrehozására (kf. online elérhető bejegyzése \cite{dpdk-logging} alapján).
Ezek a kategóriák a \ref{src:dpdk-logtypes} számú kódrészletben vannak felsorolva.

\bigskip

\lstset{caption={Host Log: DPDK logtípusok.}, label=src:dpdk-logtypes}
\begin{lstlisting}[language={C}]
#define RTE_LOGTYPE_USER1 24 
#define RTE_LOGTYPE_USER2 25 
#define RTE_LOGTYPE_USER3 26 
#define RTE_LOGTYPE_USER4 27 
#define RTE_LOGTYPE_USER5 28 
#define RTE_LOGTYPE_USER6 29 
#define RTE_LOGTYPE_USER7 30 
#define RTE_LOGTYPE_USER8 31 
\end{lstlisting}

DPDK-ban hasonlóan vannak definiálva a log szintek is, melyek a \ref{src:dpdk-loglevels} számú kódrészletben olvashatóak.

\bigskip

\lstset{caption={Host Log: DPDK log szintek.}, label=src:dpdk-loglevels}
\begin{lstlisting}[language={C}]
#define RTE_LOG_EMERG    1U
#define RTE_LOG_ALERT    2U
#define RTE_LOG_CRIT     3U
#define RTE_LOG_ERR      4U
#define RTE_LOG_WARNING  5U
#define RTE_LOG_NOTICE   6U
#define RTE_LOG_INFO     7U
#define RTE_LOG_DEBUG    8U
\end{lstlisting}

Ezek a forráskódban saját makrókkal használatosak melynek egy példája az \ref{src:log-app} számú kódrészletben definiált általános applikáció típus.

\bigskip

\lstset{caption={Host Log: általános makró.}, label=src:log-app}
\begin{lstlisting}[language={C}]
#define RTE_LOGTYPE_APP RTE_LOGTYPE_USER1
#define LOG(level, fmt, args...) \
    rte_log(RTE_LOG_ ## level, RTE_LOGTYPE_APP, "APP: " fmt, ## args)
\end{lstlisting}

\bigskip

További log típusok is léteznek a kódban, melyek a kód különböző részeihez tartoznak.
Ilyenek a tűzfalhoz és a recent modulhoz tartozó log makrók.

A tűzfalhoz kapcsolódó logolást a \ref{src:log-firewall} számú kódrészletben definiált makróval kell elvégezni.

\bigskip

\lstset{caption={Host Log: tűzfal makró.}, label=src:log-firewall}
\begin{lstlisting}[language={C}]
#define RTE_LOGTYPE_GPU_FW RTE_LOGTYPE_USER2
#define FW_LOG(level, fmt, args...) \
    rte_log(RTE_LOG_ ## level, RTE_LOGTYPE_GPU_FW, "GPU_FW: " fmt, ## args)
\end{lstlisting}

\bigskip

A recent modulhoz kapcsolódó logolást a \ref{src:log-module-recent} számú kódrészletben definiált makróval kell elvégezni.

\bigskip

\lstset{caption={Host Log: recent modul makró.}, label=src:log-module-recent}
\begin{lstlisting}[language={C}]
#define DEFAULT_LIST_NAME "default"
#define RC_LOG(level, fmt, args...) \
    rte_log(RTE_LOG_ ## level, RTE_LOGTYPE_GPU_FW, "MODULE_RECENT: " fmt, ## args)
\end{lstlisting}

Ezek a host logok, a \texttt{gpu-dpdk-log.txt} fájlba íródnak, amely a futtatáskor jön létre, és minden futtatáskor felülíródik.

A log típusok szintjeinek beállítására a \texttt{-{}-log-level <log kategória>:<log szint>} EAL paraméter használható.
Az összes kategória beállításához használható a \enquote{*} helyettesítő karakter.


% =========================================
%                SUBSECTION
% =========================================
\subsection{Device logolás}

A device kódok logolása hasonlóan makrókkal történnek, azonban ezek a standard kimenetre íródnak ki.
Három szintje van a logolásnak, és ezeknek a beállításához a következő változókat lehet deklarálni a kódban (\texttt{common.h}-ban):

\lstset{caption={Device Log: log szintek.}, label=src:device-log-levels}
\begin{lstlisting}[language={C}]
#define KERNEL_LOG_LEVEL_DEBUG
#define KERNEL_LOG_LEVEL_INFO
#define KERNEL_LOG_LEVEL_ERR
\end{lstlisting}

\bigskip

A használandó device log makró végül egy egyszerű \texttt{printf()} hívásra redukálódik, amely az \ref{src:device-log-macro} számú kódrészletben van definiálva.

\bigskip

\lstset{caption={Device Log: makró.}, label=src:device-log-macro}
\begin{lstlisting}[language={C}]
#define KLOG(level, fmt, args...) KLOG_##level(fmt, ##args)
\end{lstlisting}

\begin{note}
A \ref{src:device-log-macro} számú kódrészletben definiált makróban a \texttt{level} paraméter \texttt{DEBUG}, \texttt{INFO} vagy \texttt{ERR} értékek lehetnek.
\end{note}

% =========================================
%                 SECTION
% =========================================
\section{Hibakeresés és profilozás}

Hibakeresést és program kód profilozását az Nvidia által rendelkezésünkre álló külső programokkal (cuda-gdb és nvprof) tehetőek meg.


% =========================================
%                SUBSECTION
% =========================================
\subsection{Hibakeresés}

A program hibakeresése megtehető a megszokott \texttt{GDB} programmal, azonban az nem tudja a device kódokat részletesen elemezni.
Jobb eredményt ad, ha a direkt erre a célra fejlesztett \texttt{CUDA-GDB} programot használjuk.
Ennek a programnak a használata hasonló a megszokott \texttt{GDB} programéval.
A program \texttt{CUDA-GDB}-vel való futtatását a \ref{src:debugging} számú parancs szemlélteti.

\bigskip

\lstset{caption={Hibakeresés: cuda-gdb használata.}, label=src:debugging}
\begin{lstlisting}[language={Bash}]
sudo <CUDA eleresi utvonal>/bin/cuda-gdb --args gpu-dpdk/build/examples/dpdk-gpu-packet-processor <program argumentumok>
\end{lstlisting}


% =========================================
%                SUBSECTION
% =========================================
\subsection{Profilozás}

A device kódok profilozásához az \texttt{nvprof} alkalmazás vehető használatba.
A program \texttt{nvprof}-al való futtatását az \ref{src:profiling} számú parancs mutatja be.

\bigskip

\lstset{caption={Profilozás: nvprof használata.}, label=src:profiling}
\begin{lstlisting}[language={Bash}]
sudo <CUDA eleresi utvonal>/bin/nvprof gpu-dpdk/build/examples/dpdk-gpu-packet-processor <program parameterek>
\end{lstlisting}

A futás végén részletes statisztikákat kapunk különböző CUDA metódus hívásokról, GPU memória használatról és a programozó által definiált NVIDIA Tools Extension (NVTX) (Jiri Kraus online cikke \cite{cuda-nvtx} alapján) szakaszokról, ha azok aktiválva vannak. 

Az NVTX funkciót a \texttt{PROFILE\_NVTX\_RANGES} preprocesszor változó \texttt{common.h}-ban történő definiálásával lehet bekapcsolni.
Ha nem definiáljuk az előbb említett változót, az NVTX funkciók nem lesznek belefordítva a programkódba, így az \texttt{nvprof} sem generál velük kapcsolatos kimenetet.

\bigskip

Egy NVTX szakaszt a programban a \texttt{PUSH\_RANGE(név, színkód)}, ahol a név paraméter a szakasz neve, a színkód pedig a szakasz színét specifikálja (csak NVIDIA Visual Profiler GUI program használata esetén) és a \texttt{POP\_RANGE} makrókkal lehet definiálni. Lásd: \ref{src:nvtx-macros} kódrészlet.

\bigskip

\lstset{caption={Profilozás: NVTX szakaszok definiálása.}, label=src:nvtx-macros}
\begin{lstlisting}[language={C}]
PUSH_RANGE("rte_eth_rx_burst", 1);
...
POP_RANGE;
\end{lstlisting}

\bigskip

Ekkor az nvprof az \ref{src:nvtx-output}-ban látható eredményhez hasonló kimenetet állít elő.

\bigskip

\lstset{caption={Profilozás: NVTX szakaszok kimenete nvprof-ban.}, label=src:nvtx-output}
\begin{lstlisting}[language={C}]
==84366==       Range "rte_eth_rx_burst"
Type   Time(%)      Time Calls       Avg       Min       Max
Range: 100.00%  3.40898s     1  3.40898s  3.40898s  3.40898s
No kernels were profiled in this range.
No API activities were profiled in this range.
\end{lstlisting}


% =========================================
%                 SECTION
% =========================================
\section{Scapy szkriptek}
\label{ch:scapy}

Scapy egy Python csomag manipulációs könyvtár, melynek segítségével könnyedén generálhatunk saját csomagokat elküldésre, illetve olvashatunk (\enquote{sniff}) bejövő csomagokat.
Ezek a szkriptek a hivatalos Scapy dokumentáció \cite{scapy-docs} és Bharath Kumar The Art of Packet Crafting with Scapy című online könyve \cite{scapy-packet-crafting} segítségével készültek.

Scapy felhasználásával készült néhány Python szkript, amelyek a tesztelést segítik. 
Ezek a \texttt{\detokenize{gpu-dpdk/scapy_scripts}} könyvtárban találhatóak.
Futtatásukhoz Scapy, Python 3.0+ és adminisztrátori jog szükséges.

Scapy és Python telepítése Ubuntu alapú operációs rendszereken a \ref{src:scapy-dependencies}-ban leírt parancsok szerint történik.

\bigskip

\lstset{caption={Scapy: dependenciák telepítése.}, label=src:scapy-dependencies}
\begin{lstlisting}[language={C}]
sudo apt install python3
sudo apt install python3-scapy
\end{lstlisting}


% =========================================
%                SUBSECTION
% =========================================
\subsection{\texttt{throughput.py}}

Ez a szkript egyszerű teljesítmény tesztelésre alkalmas. 
Habár Python teljesítmény szempontjából nem a legjobb választás, több szálon történő futtatással, és a Scapy \texttt{sendpfast} függvény használatával, amely tcpreplay-t használ, kielégítő csomaggenerálási teljesítményt nyújt.
A DPDK által rendelkezésünkre álló \texttt{testpmd} program is használható nagyobb teljesítményű csomaggenerálásra, azonban ennek hátránya a program komplexitásából adódó nehezebb használat, és mivel a \texttt{testpmd} egy DPDK applikáció, az általa használt NIC-t DPDK-hoz kell rendelni, így más program egyidejűleg nem használhatja ezt a NIC-t, és attól függően hogy milyen hálózati kontrollert használunk, kell konfigurálni a hozzárendelést.

\bigskip
Használata:

\lstset{caption={\texttt{throughput.py}: szkript használata.}, label=src:scapy-throughput-help}
\begin{lstlisting}[language={Bash}]
usage: throughput.py [-h] [-p PACKETS] [-q QUEUES] [-i INTERFACE] [-t THREADS] [--payload-size PAYLOAD_SIZE]

optional arguments:
  -h, --help
  -p PACKETS, --packets PACKETS
  -q QUEUES, --queues QUEUES
  -i INTERFACE, --interface INTERFACE
  -t THREADS, --threads THREADS
  --payload-size PAYLOAD_SIZE
\end{lstlisting}


% =========================================
%                SUBSECTION
% =========================================
\subsection{\texttt{port\_knocking.py}}

Ez a szkript a port knocking konfiguráció teszteléséhez használható.
A program 4 burst csomagot generál.
Az első hármat UDP csomagok alkotják, amelyek rendre kopogtatják az 12345, 23456, 34567 portokat.
Végül a negyedik burst TCP csomagokból áll, amelyekből egy a 22-és portot célozza.

Teszteléshez a DPDK applikációban kell bekapcsolni a device logolást, majd a kimenet alapján lehet végigkövetni, hogy a port knocking megfelelően zajlott-e le.

\bigskip
Használata:

\lstset{caption={\texttt{port\_knocking.py}: szkript használata.}, label=src:scapy-port-knocking-help}
\begin{lstlisting}[language={Bash}, float, floatplacement=H]
usage: port_knocking.py [-h] [-r REPEATS] [-i INTERFACE]

optional arguments:
  -h, --help
  -r REPEATS, --repeats REPEATS
  -i INTERFACE, --interface INTERFACE
\end{lstlisting}


% =========================================
%                SUBSECTION
% =========================================
\subsection{\texttt{firewall\_test.py}}

Ez a szkript a lehető legtöbb kombinációban generál csomagokat, általános tűzfal konfiguráció teszteléshez.
UDP és TCP csomagokat generál melyekhez változó cél és forrás portok tartoznak.
A cél IP tartomány a \texttt{10.0.2.0/30}.

A futtatás végén a szkript kiírja hogy mely csomagok nem érkeztek vissza.
Ezeket a csomagokat a DPDK applikáció eldobta.

\bigskip
Használata:

\lstset{caption={\texttt{firewall\_test.py}: szkript használata.}, label=src:scapy-firewall-test-help}
\begin{lstlisting}[language={Bash}]
usage: firewall_test.py [-h] [-r REPEATS] [-i INTERFACE] [-f FILTER]

optional arguments:
  -h, --help
  -r REPEATS, --repeats REPEATS
  -i INTERFACE, --interface INTERFACE
  -f FILTER, --filter FILTER
\end{lstlisting}


% =========================================
%                 SECTION
% =========================================
\section{Tesztelés}

\begin{center}
	\begin{longtable}{ | p{0.7\textwidth} | p{0.3\textwidth} | }
		
		\hline
		\multicolumn{2}{|c|}{\textbf{Tesztelési jegyzőkönyv}}
		\\ \hline
		
		\emph{Teszt eset} & \emph{Elvárt kimenet}
		\\ \hline \hline
		\endfirsthead % első oldal fejléce
		
		\hline
		\emph{Teszt eset} & \emph{Elvárt kimenet}
		\\ \hline \hline
		\endhead % többi oldal fejléce
		
		\hline
		\endfoot % többi oldal lábléce
		
		\endlastfoot % utolsó oldal lábléce
		
		\emph{\texttt{-b <burst méret>} paraméter értéke kisebb mint egy.}
		& A program hibaüzenettel terminál.
		\\ \hline
		
		\emph{\texttt{-c <magok száma>} paraméter értéke nem páros, vagy egy.}
		& A program hibaüzenettel terminál.
		\\ \hline
		
		\emph{\texttt{-g <GPU sorszáma>} paraméter értéke nem értelmezhető (nincs ilyen sorszámú GPU).}
		& A program hibaüzenettel terminál.
		\\ \hline
		
		\emph{\texttt{-s <CUDA streamek száma>} paraméter értéke nem esik a \texttt{[0, 64]} intervallumba.}
		& A program hibaüzenettel terminál.
		\\ \hline
		
		\emph{\texttt{-P <teljesítmény csomagok száma> paraméter értéke negatív.}}
		& A program hibaüzenettel terminál.
		\\ \hline
		
		\emph{\texttt{-W <bemelegítő csomagok száma> paraméter értéke negatív.}}
		& A program hibaüzenettel terminál.
		\\ \hline

		\emph{\texttt{–firewall-config-file <útvonal>} paraméterben megadott fájl nem megnyitható, vagy a fájl érvénytelen parancsot tartalmaz, az nem értelmezhető.}
		& A program hibaüzenettel terminál.
		\\ \hline
		
		\emph{Nem adunk meg tűzfal konfigurációt.}
		& A program működése zavartalanul folytatódik, szűrés nélkül, azaz minden csomag átengedésre kerül.
		\\ \hline
		
		\emph{A tűzfal egyszerű beállításainak tesztelése Scapy szkriptek segítségével.}
		& Scapy szkript kimenetének vizsgálatával eldönthető, hogy a program megfelelően működött-e.
		\\ \hline
		
		\emph{A tűzfal komplex (port knocking) beállításainak tesztelése Scapy szkriptek segítségével.}
		& Port knocking Scapy szkript kimenetének vizsgálatával eldönthető, hogy a program megfelelően működött-e.
		\\ \hline

		\emph{A tűzfal konfigurációban érvénytelen lánc nevet adunk meg.}
		& A program hibaüzenettel terminál.
		\\ \hline

		\emph{A tűzfal konfigurációban érvénytelen protokoll típust adunk meg.}
		& A program hibaüzenettel terminál.
		\\ \hline

		\emph{A tűzfal konfigurációban érvénytelen IP címet adunk meg.}
		& A program hibaüzenettel terminál.
		\\ \hline

		\emph{A tűzfal konfigurációban 32 karakternél hosszabb lánc nevet adunk meg.}
		& A program hibaüzenettel terminál.
		\\ \hline

		\emph{A tűzfal konfigurációban 32 karakternél hosszabb recent lista nevet adunk meg.}
		& A program hibaüzenettel terminál.
		\\ \hline

		\caption{Tesztelési jegyzőkönyv}
		\label{tab:test}		
	\end{longtable}
\end{center}